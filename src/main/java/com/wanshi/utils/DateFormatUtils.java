package com.wanshi.utils;

import java.text.DecimalFormat;
import java.util.Calendar;

public class DateFormatUtils {

    public static void main(String[] args) {
        System.out.println(format());
    }

    public static String format(){

        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);

        DecimalFormat df = new DecimalFormat("00");
        String month = df.format(calendar.get(Calendar.MONTH)+1);
        String day = df.format(  calendar.get(Calendar.DAY_OF_MONTH));

        return year+month+day;
    }
}
