package com.wanshi.utils;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Data
@Component
public class AliyunProperties implements Serializable {

    /**
     * oss的端点信息
     */
    @Value("${aliyun.endpoint}")
    private String endpoint;

    @Value("${aliyun.accessKeyId}")
    private String accessKeyId;

    @Value("${aliyun.accessKeySecret}")
    private String accessKeySecret;

    /**
     * 存储空间名称
     */
    @Value("${aliyun.bucketName}")
    private String bucketName;
    /**
     * Buckect名称, 访问文件时作为基础URL
     */
    @Value("${aliyun.bucketDomain}")
    private String bucketDomain;

    @Value("${aliyun.dirName}")
    private String dirName;

}