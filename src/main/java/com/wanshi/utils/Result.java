package com.wanshi.utils;

public class Result {

    private Integer code;

    private String msg;

    private Object data;

    public Object getData() {
        return data;
    }

    public Result(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Result(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Result() {
    }

    public Result(Integer code) {
        this.code = code;
    }

    public static Result ok(){
        return new Result(Constants.SUCCESS,"成功");
    }

    public static  Result error(){
        return new Result(Constants.ERROR,"失败");
    }

    public static  Result ok(Object obj){
        return new Result(Constants.SUCCESS,"成功",obj);
    }
}
