package com.wanshi.service.impl;

import com.wanshi.entity.User;
import com.wanshi.mapper.UserMapper;
import com.wanshi.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 顽石教育-www.wanshi.com
 * @since 2021-12-08
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
