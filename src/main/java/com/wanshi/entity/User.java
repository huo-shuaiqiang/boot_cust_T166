package com.wanshi.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author 顽石教育-www.wanshi.com
 * @since 2021-12-08
 */
@TableName(autoResultMap = true)
@ApiModel
@Data
public class User implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("用户名称")
    private String username;


    @ApiModelProperty("密码")
    private String password;

    private String gender;

    @TableField(typeHandler = JacksonTypeHandler.class)
    private String[] hobby;

    //private String hobby;

    private String city;

    private String picurl;

    private String introduce;


}
