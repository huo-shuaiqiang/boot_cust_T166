package com.wanshi.mapper;

import com.wanshi.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 顽石教育-www.wanshi.com
 * @since 2021-12-08
 */
public interface UserMapper extends BaseMapper<User> {

}
