package com.wanshi.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wanshi.entity.User;
import com.wanshi.service.IUserService;
import com.wanshi.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 顽石教育-www.wanshi.com
 * @since 2021-12-08
 */
@CrossOrigin
@Api(tags = "我是UserController,222222")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @ApiOperation(value="根据id查询用户")
    @ApiImplicitParam(name = "id",value = "用户id",dataType = "string",paramType = "path",dataTypeClass = String.class)
    @GetMapping("/{id}")
    public Result findById(@PathVariable String id){
        return Result.ok(userService.getById(id));
    }

    /*@ApiOperation(value="查询所有用户")
    @GetMapping("/list")
    public Result list(){
        return Result.ok(userService.list());
    }*/

   /* name：参数名
    value：参数的汉字说明、解释
    required：参数是否必须传
    paramType：参数放在哪个地方
            · header --> 请求参数的获取：@RequestHeader
            · query --> 请求参数的获取：@RequestParam   url?username=zhangsan&password=3333
            · path（用于restful接口）--> 请求参数的获取：@PathVariable   url/zhangsan/3333
            · div（不常用）
            · form（不常用）
    dataType：参数类型，默认String，其它值dataType="Integer"*/
    @ApiOperation(value="根据用户名和密码查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name="username",value = "用户名",paramType = "path",dataType="string",dataTypeClass = String.class),
            @ApiImplicitParam(name="password",value = "密码",paramType = "path",dataType="string",dataTypeClass = String.class)
    })
    @GetMapping("/find/{password}/{username}")
    public Result findByNameAndPsd(@PathVariable String username,
                                 @PathVariable String password){

        QueryWrapper<User> wraper = new QueryWrapper<>();
        wraper.eq("username",username);
        wraper.eq("password",password);
        return Result.ok(userService.getOne(wraper));
    }

    //带条件的分页
    @ApiOperation("带条件的分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name="ye",value = "页码",paramType = "path",dataType="int",dataTypeClass = Integer.class)
            ,@ApiImplicitParam(name="size",value = "条出",paramType = "path",dataType="int",dataTypeClass = Integer.class)
    })
    @PostMapping("/page/{ye}/{size}")
    public Result findByPage(@PathVariable int ye,@PathVariable int size
    ,@RequestBody User user){
        Page page = new Page(ye,size);

        //1 姓名 模糊查询  性别  city
        QueryWrapper<User> wraper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(user.getUsername())){
            wraper.like("username",user.getUsername());
        }
        if(!StringUtils.isEmpty(user.getGender())){
            wraper.eq("gender",user.getGender());
        }
        if(!StringUtils.isEmpty(user.getCity())){
            wraper.eq("city",user.getCity());
        }
        wraper.orderByDesc("id");
        userService.page(page,wraper);
        return Result.ok(page);
    }

    //新增用户
    @ApiOperation("新增用户")
    @PostMapping
    public Result addUser(@RequestBody User user){
        if(StringUtils.isEmpty(user.getPassword())){
            user.setPassword("333333");
        }
        userService.saveOrUpdate(user);
        return Result.ok();
    }


    @ApiOperation("删除用户")
    @ApiImplicitParam(name = "id",value = "用户id",dataType = "string",paramType = "path",dataTypeClass = String.class)
    @DeleteMapping("/{id}")
    public Result delUser(@PathVariable String id){
        userService.removeById(id);
        return Result.ok();
    }


    @ApiOperation("修改用户")
    @PutMapping
    public Result delUser(@RequestBody User user){
        userService.updateById(user);
        return Result.ok();
    }


    @ApiOperation("批量删除用户")
    @PostMapping("/batch")
    public Result delBatch(@RequestBody List<String> ids){
        userService.removeByIds(ids);
        return Result.ok();
    }



}

